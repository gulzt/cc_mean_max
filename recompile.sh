#rebuild the outsmart Generator
folder=src
target=gen_random_weights
bazel build //$folder:$target

#rebuild the fitness function
g++ -std=c++14 -O3 -o fitness.exe src/fitness.cpp src/fitness_functions.cpp && echo "rebuild fitness"