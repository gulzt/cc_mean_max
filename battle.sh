#!/bin/bash
cpu_cores=3
rounds_per_group=50
subpool_count=30
middle_layer_neuron_count=34
generation=0
time_out_time=10

while getopts ":c:g:m:p:r:s:" opt; do
    case $opt in 
        c)
            cpu_cores=$OPTARG
            ;;
        g)
            generation=$OPTARG
            ;;
        m)
            middle_layer_neuron_count=$OPTARG
            ;;
        p)
            pool_size=$OPTARG
            ;;
        r)
            rounds_per_group=$OPTARG
            ;;
        s)
            subpool_count=$OPTARG
            ;;
        \?)
            echo "invalid option: - $OPTARG" >&2
            exit 2
            ;;
    esac
done

nn_type="34-${middle_layer_neuron_count}-3"
folder=pool/gen${generation}
time_out_time=$((rounds_per_group * 2))

#clean slate
rm -f "${folder}/results.dat"
rm -f "results.dat"

echo "$subpool_count subpools, each bot plays $rounds_per_group matches"
echo "three competitors at the same time"
echo "$((subpool_count*rounds_per_group*6)) matches in total"
sleep 3s
# A-B-C
# D-E-F
# G-H-I
# A-D-G
# B-E-H
# C-F-I

#subpool members
mA=1
mB=2
mC=3
mD=4
mE=5
mF=6
mG=7
mH=8
mI=9

#functions
play_round_gen0 () {
  echo "g${1} g${2} g${3}" >> "results.dat"
  while
    ! timeout $time_out_time java -jar brutaltester.jar -r "java -jar cg-mm.jar" \
    -p1 "${folder}/g${1}.exe" \
    -p2 "${folder}/g${2}.exe" \
    -p3 "${folder}/g${3}.exe" \
    -t ${cpu_cores} -n ${rounds_per_group}
  do 
    echo "battle g${1} vs g${2} vs g${3} hang"
    sed -i "/g${1} g${2} g${3}/q" results.dat
    echo "restarting brutaltester for this group"
  done
}

play_round () {
  echo "g${1} g${2} g${3}" >> "results.dat"
  while
    ! timeout $time_out_time java -jar brutaltester.jar -r "java -jar cg-mm.jar" \
    -p1 "${folder}/g${generation}_${nn_type}_father.exe pass $generation $4 $pool_size n" \
    -p2 "${folder}/g${generation}_${nn_type}_father.exe pass $generation $5 $pool_size n" \
    -p3 "${folder}/g${generation}_${nn_type}_father.exe pass $generation $6 $pool_size n" \
    -t ${cpu_cores} -n ${rounds_per_group}
  do 
    echo "battle g${1} vs g${2} vs g${3} hang"
    sed -i "/g${1} g${2} g${3}/q" results.dat
    echo "restarting brutaltester for this group"
  done
}

#fight
if [ $generation = 0 ]; then
    for (( i=1; i<=${subpool_count}; ++i)); do
        play_round_gen0 ${generation}_${nn_type}_${mA} ${generation}_${nn_type}_${mB} ${generation}_${nn_type}_${mC}
        play_round_gen0 ${generation}_${nn_type}_${mD} ${generation}_${nn_type}_${mE} ${generation}_${nn_type}_${mF}
        play_round_gen0 ${generation}_${nn_type}_${mG} ${generation}_${nn_type}_${mH} ${generation}_${nn_type}_${mI}
        play_round_gen0 ${generation}_${nn_type}_${mA} ${generation}_${nn_type}_${mD} ${generation}_${nn_type}_${mG}
        play_round_gen0 ${generation}_${nn_type}_${mB} ${generation}_${nn_type}_${mE} ${generation}_${nn_type}_${mH}
        play_round_gen0 ${generation}_${nn_type}_${mC} ${generation}_${nn_type}_${mF} ${generation}_${nn_type}_${mI}

        ((mA+=9));((mB+=9));((mC+=9));((mD+=9));((mE+=9));((mF+=9));((mG+=9));((mH+=9));((mI+=9))
    done
else
    for (( i=1; i<=${subpool_count}; ++i)); do
        play_round ${generation}_${nn_type}_${mA} ${generation}_${nn_type}_${mB} ${generation}_${nn_type}_${mC} ${mA} ${mB} ${mC}
        play_round ${generation}_${nn_type}_${mD} ${generation}_${nn_type}_${mE} ${generation}_${nn_type}_${mF} ${mD} ${mE} ${mF}
        play_round ${generation}_${nn_type}_${mG} ${generation}_${nn_type}_${mH} ${generation}_${nn_type}_${mI} ${mG} ${mH} ${mI}
        play_round ${generation}_${nn_type}_${mA} ${generation}_${nn_type}_${mD} ${generation}_${nn_type}_${mG} ${mA} ${mD} ${mG}
        play_round ${generation}_${nn_type}_${mB} ${generation}_${nn_type}_${mE} ${generation}_${nn_type}_${mH} ${mB} ${mE} ${mH}
        play_round ${generation}_${nn_type}_${mC} ${generation}_${nn_type}_${mF} ${generation}_${nn_type}_${mI} ${mC} ${mF} ${mI}

        ((mA+=9));((mB+=9));((mC+=9));((mD+=9));((mE+=9));((mF+=9));((mG+=9));((mH+=9));((mI+=9))
    done
fi

mv "results.dat" "${folder}/results.dat"
echo "done with battle!"
sleep 1

./fitness.exe $rounds_per_group $subpool_count $folder