#!/bin/bash

while getopts ":o:m:" opt; do
    case $opt in 
        o)
            outFile=$OPTARG
            ;;
        m)
            middle_layer_neuron_count=$OPTARG
            ;;
        \?)
            echo "invalid option: - $OPTARG" >&2
            ;;
    esac
done

#generate the weights and inject them
bazel-bin/src/gen_random_weights $outFile.weights $middle_layer_neuron_count

#mimicks c style do_while. Code after 'do' is not evaluated except when the condition is met (evaluation of outfile failed)
while
  cp "templates/weightless_child.cpp" "$outFile.cpp"

  #inject random weights
  dd if="${outFile}.weights" of="$outFile.cpp" bs=8 seek=32 conv=notrunc
  
  #generate the binary
  g++ -O3 -std=c++14 -o "$outFile.exe" "$outFile.cpp"
  
  #evaluate success
  ! $outFile.exe "evaluate"  
do
  echo "$outFile.exe failed to evaluate. reconstructing newborn"
done
echo "generated $outFile"
