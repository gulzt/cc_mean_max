#!bin/bash
generation=0
middle_layer_neuron_count=34
while getopts ":g:m:" opt; do
    case $opt in
        g)
            generation=$OPTARG
            ;;
        m)
            middle_layer_neuron_count=$OPTARG
            ;;
        \?)
            echo "invalid option: - $OPTARG" >&2
            ;;
    esac
done
nextgen=$((generation+1))
mkdir -p pool/gen${nextgen}

index=0
folder=pool/gen${generation}
outFile=pool/gen${nextgen}/g${nextgen}_34-${middle_layer_neuron_count}-3_father

#mimicks c style do_while. Code after do is not evaluated except when the condition is met (evaluation of outfile failed)
while
  cp "templates/weightless_child.cpp" "$outFile.cpp"

  #inject father's weights
  dd if="pool/gen${generation}/g${generation}_34-${middle_layer_neuron_count}-3_father.weights" of="$outFile.cpp" bs=8 seek=32 conv=notrunc
  
  #generate the binary
  g++ -O3 -std=c++14 -o "$outFile.exe" "$outFile.cpp"
  
  #evaluate success
  ! ./$outFile.exe "evaluate"  
do
  echo "$outFile.exe failed to evaluate. reconstructing father"
done