###Outsmart
This is a work-in-process private project, where I develop an ecosystem to train a neural network to play the 3-player game: mean-max.

###Oddities
As the game requires the delivery of the bot in a single cpp-file, the weights of the bot will have to be hardcoded in the cpp file. As this disrupts
the nature of the neural network, I choose to inject the weights in the cpp file before compilation.

###Code Quality
I took 10 days to design and code the environment in the late hours. After that I've had been fixing bugs until it ran stable. I'm aware that there is plenty of refactoring work left to do.

I use a java-version of the game that has been developed by "magusgeek" to allow for offline-play.

###How it works
I create n networks where all the connections are generated at random, expressed as a list of weights.

Bots are grouped in pools of 9 bots. Each subpool plays the following match-ups, where the number represents the id of the bot:
`1 vs 2 vs 3`
`4 vs 5 vs 6`
`7 vs 8 vs 9`
`1 vs 4 vs 7`
`2 vs 5 vs 8`
`3 vs 6 vs 9`

For every match I keep track of the match outcome and use that as input for a fitness function. The fittest player will be the "father" of the next generation of bots. In an earlier version I used crossover to produce the next generation. In the current version "the father" is mutated n times, where each bot has a higher degree of mutation. The higher the value is chosen for n, the subtler the differences are between the bots, where the first clone will be almost equal to "father".

###To Do
* Design tests
* Rewrite the offline game in C++ for better performance. 
* Embed the network generation in that application, and open the possibility for back-propagation.
* Make the network-depth a parameter. In the current version there is only a single middle layer.

###Parameters
The user can control the following parameters:

* how many cpu's to use
* rounds per match-up
* amount of sub-pools (this will create `9 * sub-pools` networks)
* amount of neurons in the middle layer
* at which generation to start (assumes you generated the networks up to n-1)


###Results so far
To win a game, a bot will need to score 50 points. A completely randomly generated neural network will score 7 points on average and the highest score I've seen in generation 0 is 19 points.

The highest score so far, with 20x as many nodes in the middle layer as in the input layer, resulted in a score of 38 points after 10 generations in a network of 8100 bots.
