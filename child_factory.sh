#!bin/bash
generation=0
nextgen=1
index=0
parentA=""
parentB=""
mutation_rate="1.0/646"

while getopts ":g:A:B:o:" opt; do
    case $opt in 
        g)
            generation=$OPTARG
            ;;
        A)
            parentA=$OPTARG
            ;;
        B)
            parentB=$OPTARG
            ;;
        o)
            outFile=$OPTARG
            ;;
        \?)
            echo "invalid option: - $OPTARG" >&2
            ;;
    esac
done

#mimicks c style do_while. Code after do is not evaluated except when the condition is met (evaluation of outfile failed)
while
  cp "$outFile.cpp" "temp.cpp"
  #inject both pairs of weights
  dd if="pool/gen${generation}/${parentB}.weights" of="$outFile.cpp" bs=8 seek=1914 conv=notrunc
  dd if="pool/gen${generation}/${parentA}.weights" of="$outFile.cpp" bs=8 seek=32 conv=notrunc
  #generate the binary
  g++ -O3 -std=c++14 -o "$outFile.exe" "$outFile.cpp"
  ! ./$outFile.exe "evaluate"  
do
  echo "$outFile.exe failed to evaluate. reconstructing outFile"
  cp "temp.cpp" "$outFile.cpp"
done

echo "$parentA and $parentB raised $outFile"