#!/bin/bash
outsmart_count=0
cpu_core=3
rounds_per_group=50
subpool_count=30
generation=0
mutation_rate="1.0/646"
middle_layer_neuron_count=34
while getopts ":c:r:s:g:m:h" opt; do
    case $opt in
        c)
            cpu_cores=$OPTARG
            ;;
        r)
            rounds_per_group=$OPTARG
            ;;
        s)
            subpool_count=$OPTARG
            ;;
        g)
            generation=$OPTARG
            ;;
        m)
            middle_layer_neuron_count=$OPTARG
            ;;
        h)
            echo "-c cpu_cores, -r rounds_per_group, -s subpool_count, -g generation"
            exit 0
            ;; 
        \?)
            echo "invalid option: - $OPTARG" >&2
            exit 2
            ;;
    esac
done

pool_size=$((subpool_count*9))
bash recompile.sh
if [ $generation = 0 ]; then
    echo "breeding the first generation"
    bash breed.sh -m $middle_layer_neuron_count -n $pool_size
else
    echo "setup for a next generation"
fi

while true; do
    echo "generation $generation"
    if [ ! $(ls pool/gen${generation}/*.cpp | wc -l) = $(ls pool/gen${generation}/*.exe | wc -l) ]; then
        echo "there are not as many exe files as cpp files for generation $generation"
        exit 1
    fi
    sleep 1
    folder=pool/gen${generation}
    bash battle.sh -c $cpu_cores -g $generation -m $middle_layer_neuron_count -p $pool_size -r $rounds_per_group -s $subpool_count
    bash selection.sh -g $generation -p $pool_size
    bash reproduce.sh -g $generation -m $middle_layer_neuron_count
    ((++generation))    
done