#!/bin/bash
bash recompile.sh
input_neuron_count=34
middle_layer_neuron_count=$input_neuron_count

while getopts ":m:n:" opt; do
    case $opt in 
        m)
            middle_layer_neuron_count=$OPTARG
            ;;
        n)
            amount=$OPTARG
            ;;
        \?)
            echo "invalid option: - $OPTARG" >&2
            exit 2
            ;;
    esac
done

mkdir -p pool/gen0
output_neuron_count=3
node_layout_id=g0_${input_neuron_count}-${middle_layer_neuron_count}-${output_neuron_count}

for (( i=1; i<=$amount; ++i)); do
    bash generate_random_AI.sh -o "pool/gen0/${node_layout_id}_$i" -m $middle_layer_neuron_count &
    ((++i))
    bash generate_random_AI.sh -o "pool/gen0/${node_layout_id}_$i" -m $middle_layer_neuron_count &
    ((++i))
    bash generate_random_AI.sh -o "pool/gen0/${node_layout_id}_$i" -m $middle_layer_neuron_count &
    wait
done

