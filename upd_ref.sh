#!/bin/bash

cd ../cg-referee-mean-max
rm *.class
rm *.jar
javac -d . src/*.java
jar cfe cg-mm.jar Referee *.class
cd -
cp ../cg-referee-mean-max/cg-mm.jar .
