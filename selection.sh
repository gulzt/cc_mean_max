#!bin/bash
generation=0
while getopts ":g:p:" opt; do
    case $opt in 
        g)
            generation=$OPTARG
            ;;
        p)
            pool_size=$OPTARG
            ;;
        \?)
            echo "invalid option: - $OPTARG" >&2
            ;;
    esac
done

#functions
create_weightfile () {
    failedCounter=0
    while [ $(cat ${new_file_name}.weights 2>/dev/null | wc -l) != 3 ]; do
        father=$(cat "${folder}/father.log")
        outsmart_id=${father: -1}
        new_file_name="${folder}/${father%_*}_father"
        if [ $generation = 0 ]; then
            cp "${folder}/${father}.exe" "${new_file_name}.exe"
            echo "${new_file_name}.exe ${new_file_name}.weights"
            ${new_file_name}.exe ${new_file_name}.weights
        else
            echo "${new_file_name}.exe ${new_file_name}.weights $generation $outsmart_id $pool_size 'y'"
            ${new_file_name}.exe ${new_file_name}.weights $generation $outsmart_id $pool_size 'y'
        fi
        if [ $failedCounter = 12 ]; then
            echo "could not create ${new_file_name}.weights"
            echo "could not create ${new_file_name}.weights" >> bad_parents.log
            exit 5
        fi
        ((++failedCounter))
    done
}

folder=pool/gen${generation}
create_weightfile

echo "I am your father $father"