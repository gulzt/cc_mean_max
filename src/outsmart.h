#pragma once

#include <exception>
#include <algorithm>
#include <vector>
#include <functional>
#include <fstream>
#include <iostream>

#include "rnd_gen.h"
class Neuron {
private:
    int layer;
    std::vector<double> weights; //weight for each connection
    std::vector<double> inputs; //the result from the activation function of each connection
    double output_value;
public:
    Neuron(int _layer, std::vector<double>&& _weights) noexcept 
        : layer(_layer), weights(_weights), inputs(_weights.size()) {}

    void feed_forward(double single_input_value, int index) {
        if (index >= inputs.size()) {
            throw std::out_of_range("index for Neuron input is out of bounds");
        }
        inputs[index] = single_input_value;
    }

    double activate() const noexcept {
        double sum = std::inner_product(begin(inputs), end(inputs), begin(weights), 0.0);
        return sum / (1 + std::fabs(sum)); //fast sigmoid
    }

    void print_weights(std::ostream& os) const {
        os << '{';
        for (int i = 0; i < weights.size() - 1; ++i) {
            os << weights[i] << ", ";
        }
        os << weights.back();
        os << '}';
    }

    double operator[](int index) {
        return weights[index];
    }
    friend std::ostream& operator<<(std::ostream& os, Neuron const& rhs);
};

std::ostream& operator<<(std::ostream& os, Neuron const& rhs) {
    os << 'l' << rhs.layer << ';';

    for (const auto& v : rhs.weights) {
        os << v << ';';
    }

    for (const auto& v : rhs.inputs) {
        os << v << ';';
    }
    return os;
}

class OutSmart {
private:
    std::vector<std::vector<Neuron>> neurons = std::vector<std::vector<Neuron>>(3);
    std::vector<double> response;
public:
    //generate a randomy weighted Outsmart
    OutSmart(int n0, int n1, int n2) : response(n2) {  // 2+1 -> 2+1 -> 3
        neurons[0].reserve(n0);
        neurons[1].reserve(n1);
        neurons[2].reserve(n2);

        //layer 0: input
        for (int i = 0; i < n0; ++i) {
            std::vector<double> weights;
            const int weight_count = 1;
            weights.reserve(weight_count);
            std::generate_n(std::back_inserter(weights), weight_count, std::bind(rnd::generate<double>, -1.0, 1.0));
            neurons[0].emplace_back(0, std::move(weights));
        }

        //layer 1: middle layer(s?)
        for (int i = 0; i < n1; ++i) {
            bool bias_node = (i == n1 - 1);
            std::vector<double> weights;
            const int weight_count = bias_node ? 1 : n0;
            weights.reserve(weight_count);
            std::generate_n(std::back_inserter(weights), weight_count, std::bind(rnd::generate<double>, -1.0, 1.0));
            neurons[1].emplace_back(1, std::move(weights));
        }
        //layer 2: output
        for (int i = 0; i < n2; ++i) {
            std::vector<double> weights;
            const int weight_count = n1;
            weights.reserve(weight_count);
            std::generate_n(std::back_inserter(weights), weight_count, std::bind(rnd::generate<double>, -1.0, 1.0));
            neurons[2].emplace_back(2, std::move(weights));
        }
    }
    OutSmart(std::vector<std::vector<std::vector<double>>>&& weight_pack) : response(weight_pack.back().size()) {
        for (int i = 0; i < weight_pack.size(); ++i) { //for each layer
            for (int j = 0; j < weight_pack[i].size(); ++j) { //for each neuron
                neurons[i].emplace_back(i, std::move(weight_pack[i][j]));
            }
        }
    }
    //reproduction constructor, default mutation_rate = 1/646 (avg of 2 weights per 34-34-3 OutSmarts)
#ifdef DEBUG_MODE
    OutSmart(std::vector<std::vector<std::vector<double>>>& parentA, 
             std::vector<std::vector<std::vector<double>>>& parentB,
#else
    OutSmart(std::vector<std::vector<std::vector<double>>>&& parentA, 
             std::vector<std::vector<std::vector<double>>>&& parentB,
#endif
             const double mutation_rate = 1.0/646) : response(parentA.back().size()) {
        for (int i = 0; i < parentA.size(); ++i) { //for each layer
            for (int j = 0; j < parentA[i].size(); ++j) { //for each neuron
                for(int w = 0; w < parentA[i][j].size(); ++w) { //for each weight
                    double mutate = rnd::generate<double>(0.0, 1.0);
                    if (mutate <= mutation_rate) {
                        double mutated_weight = rnd::generate<double>(-1.0, 1.0);
                        parentA[i][j][w] = mutated_weight; //on both, as we're
                        parentB[i][j][w] = mutated_weight; //choosing one parent anyway
                    }
                }

                int selected_parent = rnd::generate<int>(0, 1);
                if (selected_parent == 0)
                    neurons[i].emplace_back(i, std::move(parentA[i][j]));
                else {
                    neurons[i].emplace_back(i, std::move(parentB[i][j]));
                }
            }
        }
    }    

    int get_inputs_count() { return neurons[0].size(); }
    void print_heritage(std::vector<std::vector<std::vector<double>>>& parentA, 
                      std::vector<std::vector<std::vector<double>>>& parentB) {
        int parentA_gene_count=0, parentB_gene_count=0, mutation_count=0;
        int total_gene_count = 0;
        for (int i = 0; i < parentA.size(); ++i) { //for each layer
            for (int j = 0; j < parentA[i].size(); ++j) { //for each neuron
                for(int w = 0; w < parentA[i][j].size(); ++w) { //for each weight
                    if (parentA[i][j][w] == parentB[i][j][w]) {
                        ++mutation_count;
                    } else if (neurons[i][j][w] == parentA[i][j][w]) {
                        ++parentA_gene_count;
                    } else if (neurons[i][j][w] == parentB[i][j][w]) {
                        ++parentB_gene_count;
                    } else {
                        ++mutation_count;
                    }
                    ++total_gene_count;
                }
            }
        }
        std::cout << "total gene count: " << total_gene_count << '\n'
                  << "A: " << parentA_gene_count << ' ' << static_cast<double>(parentA_gene_count) / total_gene_count << '\n'
                  << "B: " << parentB_gene_count << ' ' << static_cast<double>(parentB_gene_count) / total_gene_count << '\n'
                  << "mutation: " << mutation_count << ' ' << static_cast<double>(mutation_count) / total_gene_count << '\n'
                  << "----------------------" << '\n';
    }

    auto& feed_forward(const std::vector<double>&& inputs) {
        if (inputs.size() != neurons[0].size()) {
            throw std::length_error("weight and input size for Outsmart doesn't match");
        }
        if (inputs.back() != 1.0) {
            throw std::logic_error("last node should be treated as bias node");
        }

        for (int i = 0; i < neurons[0].size(); ++i) { //for each neuron in layer 0
            neurons[0][i].feed_forward(inputs[i], 0); //pass the environmental input
            double response = neurons[0][i].activate(); //activate and store response

            for (int j = 0; j < neurons[1].size() - 1; ++j) { //minus one, for the bias
                neurons[1][j].feed_forward(response, i); //fill the specific input slot for each neuron in layer 1
            }
            auto bias_index = neurons[1].size() - 1; //treat bias neuron seperately
            neurons[1][bias_index].feed_forward(1, 0); //as it has only 1 input slot, which should be fed with value 1
        }

        for (int i = 0; i < neurons[1].size(); ++i) { //for each neuron in layer 1
            double response = neurons[1][i].activate(); //activate and store response

            for (int j = 0; j < neurons[2].size(); ++j) {
                neurons[2][j].feed_forward(response, i); //fill the specific input slot for each neuron in layer 2
            }
        }

        for (int i = 0; i < neurons[2].size(); ++i) {
            response[i] = neurons[2][i].activate(); //activate and the final responses
        }
        return response;
    }

    void log_state(std::ostream& os) {
        os << "layer 0:" << '\n';   
        for(const auto& v : neurons[0]) {
            os << v << '\n';
        }

        os << "layer 1:" << '\n';   
        for(const auto& v : neurons[1]) {
            os << v << '\n';
        }

        os << "layer 2:" << '\n';    
        for(const auto& v : neurons[2]) {
            os << v << '\n';
        }

        os << "responses:" << '\n';
        for(const auto& v : response) {
            os << v << ";" << '\n';
        }
        os << '\n';
    }
    void log_weights(std::ostream& os) {
        for(int i = 0; i < neurons.size() - 1; ++i) { //for each layer
            os << "        {";
            for(int j = 0; j < neurons[i].size() - 1; ++j) {//for each neuron
                neurons[i][j].print_weights(os);
                os << ", ";
            }
            neurons[i].back().print_weights(os);
            os << "}, \n";
        }
        os << "        {";
        for(int j = 0; j < neurons.back().size() - 1; ++j) {//for each neuron
            neurons.back()[j].print_weights(os);
            os << ", ";
        }
        neurons.back().back().print_weights(os);
        os << "}\n";
    }
};