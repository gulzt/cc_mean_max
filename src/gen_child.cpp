#include <iostream>
#include <fstream>

#include "outsmart.h"

//pass filename to pass the weights directly to a file
int main(int argc, char** argv)
{
    std::vector<std::vector<std::vector<double>>> parentA {
    };
    std::vector<std::vector<std::vector<double>>> parentB {
    };
#ifdef DEBUG_MODE
    OutSmart outsmart(parentA, parentB);
#else    
    OutSmart outsmart(std::move(parentA), std::move(parentB));
#endif
    if (argc > 1) {
        std::ofstream f(argv[1], std::ios::app);
        outsmart.log_weights(f);
    } else {
        std::ofstream f("weights.dat"); 
        outsmart.log_weights(f);
    }

#ifdef DEBUG_MODE
    outsmart.print_heritage(parentA, parentB);
#else
    std::cout << "succesfully constructed Outsmart Child!" << '\n';
#endif
}
