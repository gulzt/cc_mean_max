#include <iostream>
#include <exception>
#include <algorithm>
#include <vector>
#include <functional>
#include <random>

namespace rnd {
    std::random_device rd;   
    std::mt19937 generator(rd());
    std::uniform_int_distribution<> dist_int;
    std::uniform_real_distribution<> dist_real;
    using int_param_type_t = typename std::uniform_int_distribution<>::param_type;
    using real_param_type_t = typename std::uniform_real_distribution<>::param_type;

    template<typename T>
    inline T generate(T from, T to) {
        int_param_type_t p(from, to);
        dist_int.param(p);
        return dist_int(generator);
    }

    template<>
    inline double generate<double>(double from, double to) {
        real_param_type_t p(from, to);
        dist_real.param(p);
        return dist_real(generator);
    }
}


class Neuron {
private:
    int layer;
    std::vector<double> weights; //weight for each connection
    std::vector<double> inputs; //the result from the activation function of each connection
    double output_value;
public:
    Neuron(int _layer, std::vector<double>&& _weights) noexcept 
        : layer(_layer), weights(_weights), inputs(_weights.size()) {}

    void feed_forward(double single_input_value, int index) {
        if (index >= inputs.size()) {
            throw std::out_of_range("index for Neuron input is out of bounds");
        }
        inputs[index] = single_input_value;
    }

    double activate() const noexcept {
        double sum = std::inner_product(begin(inputs), end(inputs), begin(weights), 0.0);
        return sum / (1 + std::fabs(sum)); //fast sigmoid
    }

    void print_weights(std::ostream& os) const {
        os << '{';
        for (int i = 0; i < weights.size() - 1; ++i) {
            os << weights[i] << ", ";
        }
        os << weights.back();
        os << '}';
    }
    friend std::ostream& operator<<(std::ostream& os, Neuron const& rhs);
};

std::ostream& operator<<(std::ostream& os, Neuron const& rhs) {
    os << 'l' << rhs.layer << ';';

    for (const auto& v : rhs.weights) {
        os << v << ';';
    }

    for (const auto& v : rhs.inputs) {
        os << v << ';';
    }
    return os;
}

class OutSmart {
private:
    std::vector<std::vector<Neuron>> neurons = std::vector<std::vector<Neuron>>(3);
    std::vector<double> response;
public:
    //generate a randomy weighted Outsmart
    OutSmart(int n0, int n1, int n2) : response(n2) {  // 2+1 -> 2+1 -> 3
        neurons[0].reserve(n0);
        neurons[1].reserve(n1);
        neurons[2].reserve(n2);

        //layer 0: input
        for (int i = 0; i < n0; ++i) {
            std::vector<double> weights;
            const int weight_count = 1;
            weights.reserve(weight_count);
            std::generate_n(std::back_inserter(weights), weight_count, std::bind(rnd::generate<double>, -1.0, 1.0));
            neurons[0].emplace_back(0, std::move(weights));
        }

        //layer 1: middle layer(s?)
        for (int i = 0; i < n1; ++i) {
            bool bias_node = (i == n1 - 1);
            std::vector<double> weights;
            const int weight_count = bias_node ? 1 : n0;
            weights.reserve(weight_count);
            std::generate_n(std::back_inserter(weights), weight_count, std::bind(rnd::generate<double>, -1.0, 1.0));
            neurons[1].emplace_back(1, std::move(weights));
        }
        //layer 2: output
        for (int i = 0; i < n2; ++i) {
            std::vector<double> weights;
            const int weight_count = n1;
            weights.reserve(weight_count);
            std::generate_n(std::back_inserter(weights), weight_count, std::bind(rnd::generate<double>, -1.0, 1.0));
            neurons[2].emplace_back(2, std::move(weights));
        }
    }
    OutSmart(std::vector<std::vector<std::vector<double>>>&& weight_pack) : response(weight_pack.back().size()) {
        for (int i = 0; i < weight_pack.size(); ++i) { //for each layer
            for (int j = 0; j < weight_pack[i].size(); ++j) { //for each neuron
                neurons[i].emplace_back(i, std::move(weight_pack[i][j]));
            }
        }
    }

    auto& feed_forward(const std::vector<double>&& inputs) {
        if (inputs.size() != neurons[0].size()) {
            throw std::length_error("weight and input size for Outsmart doesn't match");
        }
        if (inputs.back() != 1.0) {
            throw std::logic_error("last node should be treated as bias node");
        }

        for (int i = 0; i < neurons[0].size(); ++i) { //for each neuron in layer 0
            neurons[0][i].feed_forward(inputs[i], 0); //pass the environmental input
            double response = neurons[0][i].activate(); //activate and store response

            for (int j = 0; j < neurons[1].size() - 1; ++j) { //minus one, for the bias
                neurons[1][j].feed_forward(response, i); //fill the specific input slot for each neuron in layer 1
            }
            auto bias_index = neurons[1].size() - 1; //treat bias neuron seperately
            neurons[1][bias_index].feed_forward(1, 0); //as it has only 1 input slot, which should be fed with value 1
        }

        for (int i = 0; i < neurons[1].size(); ++i) { //for each neuron in layer 1
            double response = neurons[1][i].activate(); //activate and store response

            for (int j = 0; j < neurons[2].size(); ++j) {
                neurons[2][j].feed_forward(response, i); //fill the specific input slot for each neuron in layer 2
            }
        }

        for (int i = 0; i < neurons[2].size(); ++i) {
            response[i] = neurons[2][i].activate(); //activate and the final responses
        }
        return response;
    }

    void log_state(std::ostream& os) {
        os << "layer 0:" << '\n';   
        for(const auto& v : neurons[0]) {
            os << v << '\n';
        }

        os << "layer 1:" << '\n';   
        for(const auto& v : neurons[1]) {
            os << v << '\n';
        }

        os << "layer 2:" << '\n';    
        for(const auto& v : neurons[2]) {
            os << v << '\n';
        }

        os << "responses:" << '\n';
        for(const auto& v : response) {
            os << v << ";" << '\n';
        }
        os << '\n';
    }
    void log_weights(std::ostream& os) {
        for(int i = 0; i < neurons.size() - 1; ++i) { //for each layer
            os << "        {";
            for(int j = 0; j < neurons[i].size() - 1; ++j) {//for each neuron
                neurons[i][j].print_weights(os);
                os << ", ";
            }
            neurons[i].back().print_weights(os);
            os << "}, \n";
        }
        os << "        {";
        for(int j = 0; j < neurons.back().size() - 1; ++j) {//for each neuron
            neurons.back()[j].print_weights(os);
            os << ", ";
        }
        neurons.back().back().print_weights(os);
        os << "}\n";
    }
};
namespace {
    std::vector<std::vector<std::vector<double>>> weights {
        {{0.149375}, {0.38648}, {0.985834}, {0.140468}, {0.679128}, {0.818464}, {-0.448955}, {0.974148}, {0.212263}, {-0.612638}, {-0.226611}, {-0.741847}, {0.140959}, {0.284378}, {-0.532714}, {-0.386588}, {-0.213762}, {0.137304}, {0.158389}, {-0.61799}, {-0.417587}, {-0.0261098}, {-0.397502}, {-0.358926}, {-0.769206}, {-0.742529}, {0.299704}, {0.337677}, {-0.0950628}, {0.0856894}, {-0.453884}, {0.0202146}, {-0.651391}, {0.880112}}, 
        {{-0.809302, -0.227326, 0.580008, -0.0233329, 0.442154, -0.251112, -0.858066, -0.373771, 0.171293, 0.428884, 0.564242, 0.964042, 0.539514, 0.253487, 0.76995, -0.964642, 0.317606, -0.43861, -0.64833, -0.221356, -0.71903, -0.0825694, 0.159413, 0.156332, 0.318284, -0.822472, -0.461556, 0.264717, 0.211566, -0.268262, 0.509232, 0.926949, 0.575699, -0.692481}, {-0.735895, -0.265485, -0.872584, 0.245474, -0.645824, -0.000512409, -0.578574, -0.213966, 0.435378, -0.0379705, 0.635988, -0.457411, 0.745384, -0.578156, -0.81869, 0.683846, 0.840095, -0.433717, 0.636766, 0.114973, -0.568496, 0.552433, 0.0234266, 0.327889, -0.687934, -0.220694, -0.599942, 0.0328546, -0.88323, -0.802082, 0.633606, 0.143892, -0.000974899, -0.60811}, {0.218491, 0.215219, 0.51207, 0.832596, -0.624342, -0.284862, 0.141498, -0.694376, -0.872335, 0.444986, 0.985621, 0.0703717, 0.959387, 0.67993, -0.260727, -0.436647, 0.491275, 0.872339, -0.293067, 0.264524, 0.144599, -0.380059, 0.750058, 0.772851, -0.403261, -0.691984, 0.54544, -0.368871, -0.435738, -0.848256, -0.468294, 0.189455, 0.0514397, -0.375089}, {0.631665, -0.580854, -0.528312, 0.45824, -0.102172, -0.23222, -0.347895, 0.063595, -0.12217, 0.229547, 0.484834, -0.810652, 0.843644, 0.67869, -0.203408, -0.709776, 0.17654, -0.803485, -0.969313, -0.00335141, -0.223476, -0.339027, 0.585499, -0.957388, -0.00306412, -0.898697, 0.107902, 0.809783, -0.64738, 0.462693, -0.66355, 0.64905, -0.688018, -0.289075}, {-0.246844, -0.772132, -0.529146, 0.863966, 0.691801, -0.540148, -0.915817, 0.657695, 0.159373, 0.299476, -0.144261, -0.0830186, -0.566191, -0.466337, 0.66066, 0.0315639, 0.660408, -0.176617, 0.959366, -0.798854, -0.995014, -0.505588, 0.0811382, -0.0374882, 0.322035, -0.351137, 0.303506, 0.817372, 0.0709261, -0.6433, 0.0256456, 0.275548, 0.363513, 0.479384}, {-0.94536, -0.0909598, 0.816268, 0.995407, -0.762525, -0.540235, -0.548414, 0.216962, 0.782104, 0.612063, 0.375464, 0.155234, -0.642189, 0.8232, 0.647245, 0.722265, 0.677074, -0.916494, 0.68132, -0.977058, -0.779333, -0.366567, -0.315714, 0.0332883, 0.909971, 0.627102, 0.975313, 0.0052493, 0.547748, 0.806362, 0.396443, -0.746864, 0.899582, -0.70341}, {0.114205, 0.85828, -0.350708, 0.802793, -0.669489, -0.801718, 0.774065, 0.797267, -0.806134, -0.0939688, 0.914894, -0.448096, 0.970047, 0.787735, 0.0984054, -0.507954, -0.4539, -0.857642, 0.283542, -0.841459, 0.894088, 0.175332, -0.811663, -0.186062, -0.093758, 0.810495, 0.322998, -0.989628, -0.687593, 0.471617, -0.676755, -0.525793, -0.737616, 0.800351}, {0.943488, 0.674082, -0.60577, 0.938593, -0.747367, 0.122354, -0.914716, 0.349283, 0.631637, -0.0610354, 0.310339, 0.808956, 0.212081, -0.714321, 0.179776, 0.744378, 0.127658, -0.475654, -0.766982, -0.384437, -0.130267, 0.640414, 0.51545, 0.383524, 0.905954, 0.549593, -0.0809094, 0.574188, 0.495079, -0.68302, -0.481028, -0.556682, 0.309404, 0.586959}, {0.153292, 0.238705, 0.440312, 0.365164, -0.184283, 0.0139228, -0.510447, 0.922078, 0.491722, 0.487008, 0.637409, 0.795683, -0.771096, -0.00880796, 0.235328, 0.486617, 0.318202, 0.335056, 0.269168, 0.525899, 0.593228, 0.844575, 0.939289, 0.656415, -0.284013, -0.490578, 0.987311, -0.888993, 0.711065, 0.387546, 0.338302, -0.219502, 0.424478, -0.455198}, {-0.604282, -0.634592, 0.880115, -0.260456, -0.0417171, 0.605844, 0.0148853, -0.60992, -0.734865, -0.244648, 0.933315, -0.22276, -0.297461, 0.33535, 0.808534, -0.549712, 0.974008, 0.505128, -0.463841, -0.935278, 0.768968, -0.619671, 0.595573, -0.520708, -0.268943, 0.766508, -0.928648, 0.972314, 0.842936, -0.560312, -0.568575, 0.125433, -0.0627903, -0.116326}, {-0.0772056, 0.89606, 0.360561, -0.292859, -0.542151, -0.939479, -0.734968, -0.214036, -0.875649, 0.986202, -0.827399, -0.680911, -0.0170223, 0.845218, -0.303808, -0.0550035, -0.553041, -0.246117, 0.854379, 0.463475, -0.67088, 0.161475, 0.119937, -0.76749, 0.641718, -0.317576, 0.575852, 0.986646, -0.134974, 0.826326, 0.259262, -0.899752, 0.0687647, 0.668363}, {-0.343532, 0.656569, -0.987236, 0.66844, -0.103185, -0.0946251, 0.855903, 0.851956, 0.711548, 0.310431, 0.305454, 0.387175, 0.0254268, 0.973144, -0.355167, -0.867115, -0.628566, -0.644154, -0.553556, -0.657062, -0.802315, -0.509022, 0.554267, 0.494435, -0.782931, 0.191805, -0.832235, -0.847094, 0.0997963, -0.863415, 0.384684, 0.134072, 0.607309, -0.865402}, {0.765573, -0.63177, -0.471287, 0.057908, 0.910371, 0.449103, 0.482805, -0.160618, -0.753025, -0.825785, 0.469524, 0.655364, 0.85255, -0.411689, -0.456541, 0.718377, -0.44132, -0.694104, -0.335046, 0.0294542, 0.0616374, 0.531553, 0.376099, -0.823564, -0.691264, -0.416172, 0.918289, -0.869332, -0.5492, -0.670327, 0.147347, 0.970705, -0.756575, -0.72566}, {0.629472, -0.484942, -0.163926, -0.83014, 0.679096, -0.430113, -0.186747, -0.755613, 0.800061, 0.360216, -0.162398, 0.337957, -0.704231, -0.256243, 0.271314, -0.646472, 0.729734, 0.748504, -0.683993, 0.888594, 0.806568, 0.21439, -0.900916, -0.701574, 0.311913, -0.583789, -0.217764, -0.694538, 0.0720149, -0.294227, 0.27184, -0.0945061, 0.00758676, -0.891271}, {-0.517216, 0.936424, 0.702836, 0.972508, 0.262135, 0.935739, -0.549319, -0.553017, -0.00071115, -0.729835, -0.348415, 0.450434, 0.503519, 0.0655117, 0.312707, -0.452013, 0.95041, -0.496602, 0.397659, 0.870784, -0.189943, 0.163621, 0.737334, 0.877119, 0.311818, 0.112533, -0.693536, -0.894574, -0.346435, 0.234175, -0.619567, -0.557792, -0.0637227, -0.310677}, {-0.598152, -0.118495, -0.998931, 0.435347, 0.797032, 0.103889, -0.380403, 0.562755, -0.770599, 0.28085, 0.155016, 0.969864, 0.393535, 0.675325, -0.401453, -0.100968, 0.0801269, -0.0567305, -0.115931, -0.0163181, 0.849459, 0.14957, -0.488731, -0.591596, -0.0390332, -0.598205, 0.180614, 0.603818, 0.339068, 0.757886, -0.539162, -0.0643342, -0.849025, -0.358463}, {0.389288, -0.449986, -0.210431, 0.0293926, 0.889057, -0.365349, 0.278389, 0.618128, 0.973995, -0.5038, 0.751591, -0.418893, 0.689797, -0.494327, -0.00430776, -0.884038, 0.00498616, 0.143549, -0.684925, -0.542725, -0.359688, -0.43813, 0.609607, 0.520687, 0.916169, -0.884304, 0.454636, 0.386731, -0.753449, 0.0469414, 0.162026, 0.979988, 0.0738264, 0.709242}, {0.769423, -0.391667, -0.584055, 0.280048, 0.581264, -0.351692, 0.412521, 0.067825, 0.139216, -0.131897, -0.268197, -0.769851, -0.708626, 0.121374, 0.0859508, 0.68379, 0.6005, -0.409421, 0.430706, 0.394926, -0.385295, -0.268572, -0.833327, 0.833, 0.23555, 0.340955, -0.433616, -0.300574, -0.113845, 0.734616, 0.941171, -0.0540067, -0.152602, 0.857901}, {0.9732, 0.427949, 0.362002, 0.638447, 0.488658, 0.896782, -0.219872, 0.462233, 0.118357, -0.0225161, -0.909889, 0.0171602, 0.444285, -0.849012, -0.0618218, 0.249628, 0.530258, 0.0534533, 0.59281, -0.382467, -0.00460549, -0.428066, -0.455567, -0.155784, 0.724246, -0.51548, -0.275792, -0.403948, 0.676974, 0.0952884, -0.835047, -0.799398, 0.379765, -0.319307}, {-0.856149, 0.617072, 0.593144, -0.221935, -0.962978, 0.411548, 0.139154, -0.811505, -0.249349, 0.0839184, 0.154924, -0.351998, 0.69046, -0.284303, -0.99275, 0.0544595, 0.357141, -0.0803956, -0.226758, -0.32898, 0.796279, 0.0544123, 0.143526, 0.778882, 0.74056, 0.0707877, -0.50182, -0.151919, -0.551173, -0.0937187, -0.418601, 0.65313, -0.638812, 0.641734}, {0.576133, -0.594469, -0.947695, -0.381012, 0.481704, -0.29977, 0.926627, 0.201863, 0.741429, 0.959749, 0.974881, 0.716922, 0.287687, -0.0663223, -0.579517, 0.0298334, -0.0397481, 0.483024, -0.707514, 0.861013, -0.741665, -0.735958, -0.515139, 0.00587114, -0.709002, 0.670028, -0.352397, -0.68237, -0.499076, -0.404235, -0.396351, -0.751748, -0.534911, 0.523085}, {0.681843, -0.552345, -0.0690505, -0.734434, -0.69387, 0.621238, 0.677023, -0.275588, 0.151431, -0.506889, -0.585, -0.414573, 0.907084, 0.92539, -0.445269, 0.318201, 0.55138, -0.147564, 0.152634, 0.82023, 0.0992159, 0.81425, 0.827123, 0.612494, -0.20653, 0.0793391, -0.319097, -0.433786, 0.542862, -0.728559, -0.798928, -0.921605, 0.540681, 0.870359}, {-0.111511, 0.345426, 0.973939, -0.491818, 0.441969, 0.305793, -0.744395, -0.431181, 0.416572, -0.890221, -0.0453686, 0.00214496, 0.427921, 0.0690144, 0.659108, 0.0821456, 0.478315, 0.0575277, -0.690099, -7.48292e-05, -0.498185, -0.729108, -0.624655, -0.0670375, 0.26904, 0.111459, 0.775799, -0.0197533, 0.188264, 0.483755, -0.432932, 0.804254, 0.639141, 0.147392}, {-0.118818, -0.665245, 0.580183, -0.840833, 0.694376, 0.175193, -0.493921, -0.809733, 0.605052, 0.560752, 0.190564, -0.868212, 0.938677, 0.447419, 0.340665, -0.919136, -0.62845, 0.121847, 0.766005, -0.0757814, -0.758091, 0.470809, 0.0627247, -0.905632, 0.775064, -0.545608, -0.950602, -0.285861, 0.885542, 0.416111, -0.695605, -0.0526495, 0.294929, -0.0742613}, {0.161591, -0.250593, -0.988399, 0.978087, -0.274557, -0.161181, -0.870486, -0.127564, -0.535065, -0.668074, 0.668463, -0.0832049, -0.141408, -0.37751, -0.238877, 0.883579, -0.266561, -0.419855, -0.592553, -0.0421668, 0.634651, 0.090148, -0.0340896, -0.503732, 0.419913, 0.987164, 0.212599, -0.00284208, 0.600182, 0.953631, -0.955549, 0.882134, 0.73996, -0.0106614}, {0.193963, -0.849935, -0.359038, -0.291918, 0.3385, -0.433658, 0.0942599, -0.317078, -0.203907, -0.82113, 0.430189, -0.134667, 0.794775, 0.0892903, -0.615048, 0.977827, -0.806261, 0.664313, 0.204345, -0.11757, -0.400667, -0.012842, 0.349678, -0.580872, 0.142217, 0.0585212, 0.848945, 0.616502, -0.138784, -0.801076, 0.11259, -0.108368, -0.926178, -0.490662}, {0.410427, -0.743077, 0.0498584, 0.853523, -0.522977, -0.802495, -0.246813, 0.38541, 0.460426, 0.010438, -0.818814, -0.84611, 0.396198, -0.146552, 0.207921, -0.205034, 0.958268, -0.277936, -0.735697, 0.724307, -0.631298, 0.192036, -0.744491, -0.302051, 0.217144, -0.28695, -0.108862, -0.115212, -0.650519, 0.0945129, 0.827134, 0.19648, 0.703325, 0.957727}, {-0.672609, 0.795501, -0.00308561, 0.578975, 0.00194626, -0.861586, -0.31381, 0.995813, 0.791631, -0.190935, -0.645804, 0.538383, 0.438608, 0.791136, 0.976104, -0.259759, -0.0224268, -0.747157, 0.145995, 0.0924483, -0.171099, -0.169228, 0.813325, -0.968728, -0.665355, 0.739385, -0.792984, 0.629745, 0.775383, -0.738062, -0.862911, 0.540532, 0.19955, -0.998394}, {-0.610282, 0.383693, 0.667001, -0.402361, 0.589199, -0.615922, -0.11338, 0.650235, 0.0257722, 0.237557, 0.809191, 0.839153, -0.0786647, 0.774817, -0.7384, -0.466157, 0.458909, 0.560022, -0.573204, -0.235221, -0.602135, -0.651198, 0.720263, 0.717527, -0.464238, -0.788938, 0.745179, 0.791542, 0.889547, 0.480219, 0.576842, -0.118874, 0.605525, 0.418402}, {-0.980254, -0.161428, 0.676126, -0.765559, -0.819058, 0.199255, -0.787393, -0.542622, 0.929781, 0.495409, -0.231394, -0.733161, -0.227111, 0.669075, -0.98375, 0.135669, 0.257485, 0.0932343, 0.700166, -0.992059, 0.208924, -0.276283, 0.0952789, -0.934591, 0.781027, -0.101891, -0.791776, 0.432697, 0.235387, -0.820496, -0.572724, -0.36801, 0.325674, -0.0537095}, {0.903326, -0.156223, -0.786121, 0.237221, 0.436859, 0.217218, -0.989115, 0.133353, -0.537147, 0.827841, 0.417827, 0.989254, 0.707362, 0.905128, -0.448686, 0.266947, -0.040178, -0.956605, -0.0615163, -0.648022, -0.401016, -0.199689, -0.324428, 0.205081, -0.00679307, 0.325435, 0.624727, 0.351398, 0.0696763, 0.404809, -0.449382, 0.0684326, -0.13801, -0.659216}, {-0.993346, -0.479989, -0.333383, -0.637691, -0.776188, -0.970475, -0.956969, -0.425149, 0.322654, 0.821454, -0.105226, 0.235456, -0.433361, -0.0273954, 0.370029, 0.0790136, -0.352987, 0.0495434, 0.00328166, -0.193128, -0.510664, 0.707275, 0.0323566, 0.376928, 0.493313, -0.0136073, 0.742023, 0.798875, 0.57418, 0.333162, 0.408086, -0.182408, -0.617013, 0.389308}, {-0.0121818, 0.529558, 0.11594, 0.411649, -0.590398, 0.82157, 0.729149, 0.424112, 0.755158, -0.225257, 0.676055, -0.304741, 0.804625, -0.278849, -0.0679123, 0.217668, 0.408221, 0.243439, 0.782868, 0.247823, -0.900904, 0.332742, 0.0720214, -0.0325787, -0.450166, 0.987432, 0.339847, -0.679015, 0.978605, 0.768361, 0.512792, -0.159236, -0.0454447, 0.0899888}, {-0.776453}}, 
        {{0.507932, -0.879391, -0.327238, 0.785505, 0.795756, -0.652023, 0.0420554, -0.897212, -0.629546, 0.413124, 0.247889, -0.197338, 0.306535, 0.454975, 0.305969, -0.525712, 0.227382, -0.607916, -0.452958, 0.372673, -0.110701, -0.463023, -0.843599, -0.613433, -0.881135, -0.896463, 0.714874, 0.976447, 0.848385, 0.339016, -0.0296398, 0.932839, -0.887029, 0.742578}, {0.828701, -0.97694, 0.375134, 0.636684, 0.731303, -0.445008, -0.00175563, 0.712912, -0.474526, 0.964606, -0.373286, 0.143971, -0.80055, -0.486786, -0.986596, -0.577582, 0.467741, 0.852287, -0.699415, 0.507857, -0.819314, -0.783129, 0.316395, -0.412634, -0.834472, -0.548156, 0.843962, -0.942357, 0.939148, -0.731302, -0.890647, -0.0959416, 0.703414, -0.726701}, {0.897283, 0.306121, -0.744044, 0.150743, 0.950139, -0.832774, 0.0189665, -0.364507, -0.147865, -0.151211, 0.838938, -0.329905, -0.129975, -0.470493, -0.920864, -0.202551, 0.309631, -0.217532, 0.902523, -0.300303, -0.0794655, -0.453639, 0.449382, -0.166027, 0.813195, -0.60576, 0.729232, 0.23454, -0.913933, 0.360331, -0.129056, -0.141581, 0.790282, 0.731517}}
    };
    OutSmart outsmart(std::move(weights));
}

struct Unit {
    int id;
    int type;
    int player;
    float mass;
    int radius;
    int x;
    int y;
    int vx;
    int vy;
    int extra; //water
    int extra2;
};
std::istream& operator>>(std::istream& is, Unit& u) {
    is >> u.id >> u.type >> u.player >> u.mass >> u.radius >> u.x >> u.y >> u.vx >> u.vy >> u.extra >> u.extra2; std::cin.ignore();
    return is;
}

int main() {
    while (1) {
        std::vector<double> inputs;
        inputs.reserve(weights.front().size());
        int myScore, enemyScore1, enemyScore2;
        std::cin >> myScore >> enemyScore1 >> enemyScore2; std::cin.ignore();
        int myRage, enemyRage1, enemyRage2;
        std::cin >> myRage >> enemyRage1 >> enemyRage2; std::cin.ignore();

        int unitCount;
        std::cin >> unitCount; std::cin.ignore();
        for (int i = 0; i < 3; ++i) { //players
            Unit u;
            std::cin >> u;
            inputs.push_back(u.x);
            inputs.push_back(u.y);
        }            
        for (int i = 3; i < unitCount; ++i) { //puddles
            Unit u;
            std::cin >> u;
            inputs.push_back(u.x);
            inputs.push_back(u.y);
            inputs.push_back(u.extra);
        }
        //in case there are not the maximum amount of puddles
        for (int i = unitCount; i < 12; ++i) {
            inputs.insert(std::end(inputs), {0.0, 0.0, 0.0});
        }

        //gather input
        inputs.push_back(1);

        //feed input and receive response
        auto& response = outsmart.feed_forward(std::move(inputs));
        std::cout << "MAKE IT CRASH";
        std::cout << static_cast<int>(response[0] * 3000) << ' ' 
                  << static_cast<int>(response[1] * 3000) << ' ' 
                  << static_cast<int>(150 + response[2] * 150) << ' ' 
                  << inputs.size() << '\n';
        std::cout << "WAIT" << '\n';
        std::cout << "WAIT" << std::endl;
    };
}