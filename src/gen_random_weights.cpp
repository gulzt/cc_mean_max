#include <iostream>
#include <fstream>
#include <string>

#include "outsmart.h"

//pass filename to pass the weights directly to a file
int main(int argc, char** argv)
{
    const auto input_gene_count = 34;
    auto middle_layer_neuron_count = argc == 3 ? std::stoi(argv[2]) : input_gene_count;

    std::vector<double> test_input_data;
    OutSmart outsmart(input_gene_count, middle_layer_neuron_count, 3);

    if (argc > 1) {
        std::ofstream f(argv[1], std::ios::app);
        outsmart.log_weights(f);
    } else {
        std::ofstream f("weights.dat"); 
        outsmart.log_weights(f);
    }

    std::cout << "succesfully constructed Outsmart Weights!" << '\n';
}