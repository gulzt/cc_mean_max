#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <exception>
#include <limits>
#include <array>

#include "rnd_gen.h"
#include "fitness_functions.h"

namespace fitness_results {
    std::string output_path=".";
}

std::ostream& operator<<(std::ostream& os, Player const& p) {
    os << p.name << ", "
       << p.score << ", "
       << p.weighted_score << ", fitness: "
       << p.fitness;
    return os;
}

std::vector<Player> calculate_scores(int rounds, int subpool_count, int player_count, const std::string& filename) {
    std::ifstream f_results(filename);
    std::vector<Player> players(player_count);
    auto score_fn = [](int score, int round_timer) {
        return (score * 100) / round_timer; 
    };

    int loop_counter = -1;
    for (int sp = 0; sp < subpool_count; ++sp) {
        int offset = sp * 9;
        for (int i = offset; i < 9 + offset; i += 3) {
            f_results >> players[i].name >> players[i+1].name >> players[i+2].name;
            for (int r = 0; r < rounds; ++r) {
                int round_timer;
                std::array<int, 3> score;
                f_results >> score[0] >> score[1] >> score[2] >> round_timer;
                std::transform(begin(score), end(score), begin(score), [](int score){ return std::min(score, 50);});

                players[i].score += score_fn(score[0], round_timer);
                players[i+1].score += score_fn(score[1], round_timer);
                players[i+2].score += score_fn(score[2], round_timer);
            }
        }
        for (int i = offset; i < 3 + offset; ++i) {
            f_results >> players[i].name >> players[i+3].name >> players[i+6].name;
            for (int r = 0; r < rounds; ++r) {
                int round_timer;
                std::array<int, 3> score;
                f_results >> score[0] >> score[1] >> score[2] >> round_timer;
                std::transform(begin(score), end(score), begin(score), [](int score){ return std::min(score, 50);});

                players[i].score += score_fn(score[0], round_timer);
                players[i+3].score += score_fn(score[1], round_timer);
                players[i+6].score += score_fn(score[2], round_timer);
            }
        }
    }
    for (auto& p : players) {
        if (p.score > std::numeric_limits<long double>::max()) {
            {
                std::ofstream of(fitness_results::output_path + "/errors.log", std::ios::app);
                of << p.score << " of player " << p.name << " does not fit in a double" << '\n';
            }
            throw std::overflow_error("players score overflowed!");
        }
        p.weighted_score = static_cast<long double>(p.score) / (2*rounds);            
    }

    return players;
}

void calculate_fitness(std::vector<Player>& players) {
    const double sum = std::accumulate(begin(players), end(players), 0.0, 
                                       [](double y, const Player& p) { return y + p.weighted_score;});
    const double mean = sum / players.size();

    std::vector<double> diff;
    diff.reserve(players.size());
    std::transform(players.begin(), players.end(), 
                   std::back_inserter(diff), [mean](Player& x) -> double { return x.weighted_score - mean; });

    double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
    if (sq_sum == 0.0) {
      sq_sum = 0.01;  
    } 
    const double st_dev = std::sqrt(sq_sum / players.size());

    if (st_dev == 0.0) {
        {
            std::ofstream of(fitness_results::output_path + "/errors.log", std::ios::app);
            of << "standard deviation resulted in zero, with these values: " 
               << "sum: " << sum << ", mean: " << mean << ", sq_sum: " << sq_sum << '\n';
        }
        throw std::range_error("standard deviation cannot be zero");
    }
    //sigma measurement
    //Z = (x - mean) / st_dev;
    std::transform(begin(diff), end(diff), begin(diff), [st_dev](double d) { return d / st_dev; });

    //to exponential distribution
    const double offset = *std::min_element(cbegin(diff), cend(diff));
    std::transform(begin(diff), end(diff), begin(diff), [offset](double d) { return d = std::pow(d + std::abs(offset), 3); });
    const double best_fitness = *std::max_element(cbegin(diff), cend(diff));

    for (int i = 0; i < players.size(); ++i) {
        players[i].fitness = diff[i] / best_fitness;
    }
}
 
void is_true(bool statement) {
    if (!statement) {
        throw std::logic_error("unit test was broken");
    }
}
