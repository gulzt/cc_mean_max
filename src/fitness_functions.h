#pragma once

#include <vector>

namespace fitness_results {
    extern std::string output_path;
}

//representing a pool-member
struct Player {
    std::string name;
    int score = 0;
    double weighted_score = 0.0;
    double fitness = 0.0;
    bool operator<(const Player& rhs) const {
        return this->weighted_score < rhs.weighted_score;
    }
};

std::ostream& operator<<(std::ostream& os, Player const& p);

//process data file and produce fitness scores for every member in the pool
std::vector<Player> calculate_scores(int rounds, int subpool_count, int player_count, const std::string& filename);

//calculate fitness of the entire population
void calculate_fitness(std::vector<Player>& players);

//throw exceptions on broken invariants
void is_true(bool statement);

//truncate doubles values to two digits
constexpr double double_two_digits_precision(double dd) {
    dd += (dd > 0.0 ? 0.005 : -0.005);
    dd *= 1000;
    int i = dd;
    i /= 10;
    dd = i;
    dd /= 100;
    return dd;
}