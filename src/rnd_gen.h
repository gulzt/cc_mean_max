#pragma once
#include <random>

namespace rnd {
    std::random_device rd;   
    std::mt19937 generator(rd());
    std::uniform_int_distribution<> dist_int;
    std::uniform_real_distribution<> dist_real;
    using int_param_type_t = typename std::uniform_int_distribution<>::param_type;
    using real_param_type_t = typename std::uniform_real_distribution<>::param_type;

    template<typename T>
    inline T generate(T from, T to) {
        int_param_type_t p(from, to);
        dist_int.param(p);
        return dist_int(generator);
    }

    template<>
    inline double generate<double>(double from, double to) {
        real_param_type_t p(from, to);
        dist_real.param(p);
        return dist_real(generator);
    }
}