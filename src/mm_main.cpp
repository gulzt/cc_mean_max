}

struct Unit {
    int id;
    int type;
    int player;
    float mass;
    int radius;
    int x;
    int y;
    int vx;
    int vy;
    int extra; //water
    int extra2;
};

std::istream& operator>>(std::istream& is, Unit& u) {
    is >> u.id >> u.type >> u.player >> u.mass >> u.radius >> u.x >> u.y >> u.vx >> u.vy >> u.extra >> u.extra2; std::cin.ignore();
    return is;
}

int main(int argc, char** argv) {
    if (argc==2 && strcmp(argv[1], "evaluate") == 0) {
        if (father.size() != 3) {
            return 1;
        }
        return 0;
    }

    if (argc == 2) {
         const char* weight_file_name = argv[1];
         std::ofstream f(weight_file_name);
         outsmart.log_weights(f);       
         return 0;
    }

    const int expected_argc = 6;
    if (argc > 2 && argc < expected_argc) {
        std::cerr << "user did not provide enough arguments to bot" << '\n';
        return 1;
    }

    if (argc == expected_argc) {
        const char* weight_file_name = argv[1];
        generation = std::stoi(argv[2]);
        const int outsmart_id = std::stoi(argv[3]);
        const int pool_size = std::stoi(argv[4]);
        const char is_logging_requested = argv[5][0];
        outsmart.transform_weights(generation, outsmart_id, pool_size);

        if (is_logging_requested == 'y') {
            std::ofstream f(weight_file_name);
            outsmart.log_weights(f);
        }
        return 0;
    }

    while (1) {
        std::vector<double> inputs;
        inputs.reserve(outsmart.get_inputs_count());
        int myScore, enemyScore1, enemyScore2;
        std::cin >> myScore >> enemyScore1 >> enemyScore2; std::cin.ignore();
        int myRage, enemyRage1, enemyRage2;
        std::cin >> myRage >> enemyRage1 >> enemyRage2; std::cin.ignore();

        int unitCount;
        std::cin >> unitCount; std::cin.ignore();
        for (int i = 0; i < 3; ++i) { //players
            Unit u;
            std::cin >> u;
            inputs.push_back(u.x);
            inputs.push_back(u.y);
        }            
        for (int i = 3; i < unitCount; ++i) { //puddles
            Unit u;
            std::cin >> u;
            inputs.push_back(u.x);
            inputs.push_back(u.y);
            inputs.push_back(u.extra);
        }
        //in case there are not the maximum amount of puddles
        for (int i = unitCount; i < 12; ++i) {
            inputs.insert(std::end(inputs), {0.0, 0.0, 0.0});
        }

        //gather input
        inputs.push_back(1);

        //feed input and receive response
        auto& response = outsmart.feed_forward(std::move(inputs));
        std::cout << static_cast<int>(response[0] * 3000) << ' ' 
                  << static_cast<int>(response[1] * 3000) << ' ' 
                  << static_cast<int>(150 + response[2] * 150) << ' ' 
                  << generation << '\n';
        std::cout << "WAIT" << '\n';
        std::cout << "WAIT" << std::endl;
    };
}