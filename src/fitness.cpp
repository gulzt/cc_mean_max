#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "fitness_functions.h"

namespace {
    enum {
        ROUNDS_PER_MATCHUP=1,
        TOTAL_AMOUNT_OF_SUBPOOLS=2,
        OUTPUT_PATH_OPTIONAL=3
    };
}

int main(int argc, char** argv) {
    if (argc == 1) {
        std::cout << "provide a rounds-per-matchgroup parameter" << '\n';
        return 1;
    }
    if (argc == 2) {
        std::cout << "provide the total amount of subpools" << '\n';
        return 2;
    }

    if (argc == 4) {
        fitness_results::output_path = argv[OUTPUT_PATH_OPTIONAL];
    }

    static_assert(double_two_digits_precision(3.2574131753237) == 3.26, "precision differs");
    static_assert(double_two_digits_precision(3.2544131753237) == 3.25, "precision differs");
    static_assert(double_two_digits_precision(-2.148694034573) == -2.15, "precision differs");
    static_assert(double_two_digits_precision(-2.144694034573) == -2.14, "precision differs");

    const int rounds = std::stoi(argv[ROUNDS_PER_MATCHUP]);
    const int subpool_count = std::stoi(argv[TOTAL_AMOUNT_OF_SUBPOOLS]);
    const int player_count = subpool_count * 9;

    if (rounds <= 0 || subpool_count <= 0) {
        std::cout << "don't pass zeros!" << '\n';
        return 3;
    }

    std::vector<Player> players = calculate_scores(rounds, subpool_count, player_count, fitness_results::output_path + "/results.dat");
    calculate_fitness(players);
    {
        std::ofstream f_fitness(fitness_results::output_path + "/fitness.log");
        std::sort(begin(players), end(players));
        for (auto& p : players) {
            f_fitness << p << '\n';
        }
    }
    {
        std::ofstream f_best_performer(fitness_results::output_path + "/best_performer.log");
        std::ofstream f_father(fitness_results::output_path + "/father.log");
        f_best_performer << players.front() << '\n';
        f_best_performer << players[players.size()/2] << '\n';
        f_best_performer << players.back() << '\n';
        f_father << players.back().name << '\n';
    }
}
